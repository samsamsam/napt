#include <stdio.h>
#include <libcnapt.h>
#include <string.h>

static void check(char *cmd, int expected, int provided)
{
    if (expected != provided)
    {
        printf("cmd %s need %d params, you provided %d!\n", cmd, expected, provided);
        exit(-1);
    }
}

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        printf("Please provide operation as first param!");
        exit(-1);
    }

    if (!strcmp(argv[1], "cpt_flush"))
    {
        return cpt_flush();
    }

    if (argc < 3)
    {
        printf("Please provide operation and at leas one parameter!");
        exit(-1);
    }


    if (!strcmp(argv[1], "cpm_add"))
    {
        check(argv[1], 1, argc - 2);
        return cpm_add(argv[2]);
    }
    else if (!strcmp(argv[1], "cpm_del"))
    {
        check(argv[1], 1, argc - 2);
        return cpm_del(argv[2]);
    }
    else if (!strcmp(argv[1], "cpm_edit"))
    {
        check(argv[1], 2, argc - 2);
        return cpm_edit(argv[2], argv[3]);
    }
    else if (!strcmp(argv[1], "cpm_flush"))
    {
        check(argv[1], 1, argc - 2);
        cpm_flush(argv[2]);
        return 0;
    }
    else if (!strcmp(argv[1], "cpt_add"))
    {
        check(argv[1], 1, argc - 2);
        return cpt_add(argv[2]);
    }
    else if (!strcmp(argv[1], "cpt_del"))
    {
        check(argv[1], 1, argc - 2);
        return cpt_del(argv[2]);
    }
    else if (!strcmp(argv[1], "cpt_edit"))
    {
        check(argv[1], 2, argc - 2);
        return cpt_edit(argv[2], argv[3]);
    }
    else if (!strcmp(argv[1], "cpt_flush"))
    {
        check(argv[1], 1, argc - 2);
        return cpt_flush();
    }
    else if (!strcmp(argv[1], "cnapt_flush_bypriv"))
    {
        check(argv[1], 2, argc - 2);
        cnapt_flush_bypriv(argv[2], argv[3]);
        return 0;
    }
    else if (!strcmp(argv[1], "cdmz_clean"))
    {
        check(argv[1], 2, argc - 2);
        
        cdmz_clean(strcmp(argv[2], "1") ? 1 : 0, argv[3]);
        return 0;
    }
    else
    {
        printf("Unknown command: %s\n", argv[1]);
    }
    
    return -2;
}
